#ifndef NIT_CAMERA_PRESETS
#define NIT_CAMERA_PRESETS

#include <opencv2/opencv.hpp>

enum REGISTERS
{
  SHUTTER_TRIGGER_MODES,
  PIXEL_CLOCK_FREQUENCY,
  DIGITAL_ANALOG_GAIN,
  DIGITAL_OFFSET,
  EXPOSITION,
  LINES_BLANK,
  T_BLANK,
  RES_FIRST_COLUMN, // RES means RESOLUTION
  RES_NUMBER_OF_COLUMNS,
  RES_FIRST_LINE,
  RES_NUMBER_OF_LINES,
  UNUSED_1, // Not used
  FPN_NOISE_COMPENSATION
};
typedef std::array<int, 13> RegistersParameters;

struct CaptureParameters
{
  RegistersParameters registers;
  unsigned capture_timeout_ms;
  bool has_bayer_filter;
  cv::ColorConversionCodes bayer_OpenCV_code;
};

bool getCaptureParameters(CaptureParameters &capture_params,
                          const std::string sensor_name,
                          const std::string preset = "")
{
  // White balance is way off so the image appears yellow
  capture_params.bayer_OpenCV_code = cv::COLOR_BayerBG2RGB_VNG; // Only used if has_bayer_filter is true

  if (sensor_name.find("RGB") != std::string::npos)
    capture_params.has_bayer_filter = true;
  else
    capture_params.has_bayer_filter = false;

  capture_params.registers[REGISTERS::RES_FIRST_COLUMN] = 1;
  capture_params.registers[REGISTERS::RES_NUMBER_OF_COLUMNS] = 160;
  capture_params.registers[REGISTERS::RES_FIRST_LINE] = 1;
  capture_params.registers[REGISTERS::RES_NUMBER_OF_LINES] = 128;

  if (sensor_name.find("NSC1003") != std::string::npos && (preset == "rolling_14fps" || preset.empty()))
  {
    // Stable
    // 14 FPS expected with a short USB cable
    capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES] = 1;
    capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY] = 15;
    capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN] = 0;
    capture_params.registers[REGISTERS::DIGITAL_OFFSET] = 0;
    capture_params.registers[REGISTERS::EXPOSITION] = 0;
    capture_params.registers[REGISTERS::LINES_BLANK] = 16;
    capture_params.registers[REGISTERS::T_BLANK] = 67;
    capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION] = 1;

    double frame_per_seconds = 14;
    capture_params.capture_timeout_ms = 1000 / (frame_per_seconds * 0.7);
  }
  else if (sensor_name.find("NSC1003") != std::string::npos && preset == "rolling_33fps")
  {
    // Stable
    // 33 FPS expected with a short USB cable
    capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES] = 1;
    capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY] = 0;
    capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN] = 4;
    capture_params.registers[REGISTERS::DIGITAL_OFFSET] = 0;
    capture_params.registers[REGISTERS::EXPOSITION] = 0;
    capture_params.registers[REGISTERS::LINES_BLANK] = 32;
    capture_params.registers[REGISTERS::T_BLANK] = 15;
    capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION] = 1;

    double frame_per_seconds = 33;
    capture_params.capture_timeout_ms = 1000 / (frame_per_seconds * 0.7);
  }

  else if (sensor_name.find("NSC1003") != std::string::npos && preset == "rolling_40fps")
  {
    // Not very stable (1 of 10 image is corrupted)
    // 40 FPS expected: works with short USB cable or a good active USB extension cable
    capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES] = 1;
    capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY] = 2;
    capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN] = 4;
    capture_params.registers[REGISTERS::DIGITAL_OFFSET] = 0;
    capture_params.registers[REGISTERS::EXPOSITION] = 0;
    capture_params.registers[REGISTERS::LINES_BLANK] = 45;
    capture_params.registers[REGISTERS::T_BLANK] = 34;
    capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION] = 1;

    double frame_per_seconds = 40;
    capture_params.capture_timeout_ms = 1000 / (frame_per_seconds * 0.7);
  }
  else
    return false;

  return true;
}

enum PIXEL_CLOCK_FREQUENCY_MHZ
{
  F12_5, // 12.5 MHz
  F16_666, // 16.666 MHz
  F20, // 20 MHz
  F25, // 25 MHz
  F33_333, // 33.333 MHz
  F40, // 40 MHz
  F50, // 50 MHz
  F66_666, // 66.666 MHz
  F80 // 80 MHz
};

enum ANALOG_GAIN // dB = decibels
{
  DECIBELS_0,
  DECIBELS_4,
  DECIBELS_8,
  DECIBELS_12
};

enum DIGITAL_GAIN
{
  AGC, // Automatic gain control
  G0_25, // Gain = 0.25
  G0_5, // Gain = 0.5
  G0_75, // Gain = 0.75
  G1, // Gain = 1
  G1_25,
  G1_5,
  G1_75,
  G2, // Gain = 2
  G2_25,
  G2_5,
  G2_75,
  G3, // Gain = 3
  G3_25,
  G3_5,
  G3_75,
  G4, // Gain = 4
  G4_25,
  G4_5,
  G4_75,
  G5, // Gain = 5
  G5_25,
  G5_5,
  G5_75,
  G6, // Gain = 6
  G6_25,
  G6_5,
  G6_75,
  G7, // Gain = 7
  G7_25,
  G7_5,
  G7_75,
};

enum SHUTTER_MODE
{
  GLOBAL,
  ROLLING,
  DIFFERENTIAL
};

enum TRIGGER_MODE
{
  NO_EXTERNAL_SIGNAL,
  EXTERNAL_SIGNAL,
  EXTERNAL_SIGNAL_NO_VOLTAGE_ON_TRIGGER_CONNECTOR
};

enum FIXED_PATTERN_NOISE_CORRECTION
{
  FPN_0_025,
  FPN_0_1,
  FPN_0_4,
  FPN_0_5
};

struct SensorDefinitionParameters
{
  std::string sensor_name;
  unsigned sensor_number;
  SHUTTER_MODE shutter_mode;
  unsigned sensor_width;
  unsigned sensor_height;
  unsigned image_width;
  unsigned image_height;
  unsigned first_colon;
  unsigned first_line;
  double sensor_line_time[9];
  TRIGGER_MODE trigger_mode;
  unsigned pixel_depth;
  bool has_bayer_filter; // Is it a color camera?
  cv::ColorConversionCodes bayer_OpenCV_code;
  unsigned capture_timeout_ms;
  PIXEL_CLOCK_FREQUENCY_MHZ pixel_clock_frequency;
  ANALOG_GAIN analog_gain;
  DIGITAL_GAIN digital_gain;
  unsigned digital_offset;
  FIXED_PATTERN_NOISE_CORRECTION fpn_correction;
  unsigned frames_per_second;
};

void fillCaptureParameters(const SensorDefinitionParameters sensor,
                           CaptureParameters &capture_params)
{
  int i_val;
  double d_Tlines, d_RefClk;

  // Shutter mode and trigger mode
  i_val = sensor.shutter_mode;
  i_val = i_val + 8 * (sensor.trigger_mode);
  capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES] = i_val;

  // Send pixel clock frequency
  switch (sensor.pixel_clock_frequency)
  {
    case F12_5:
      i_val = 12;
      d_Tlines = sensor.sensor_line_time[0];
      d_RefClk = 12.5;
      break;
    case F16_666:
      i_val = 14;
      d_Tlines = sensor.sensor_line_time[1];
      d_RefClk = 16.666;
      break;
    case F20:
      i_val = 15;
      d_Tlines = sensor.sensor_line_time[2];
      d_RefClk = 20;
      break;
    case F25:
      i_val = 8;
      d_Tlines = sensor.sensor_line_time[3];
      d_RefClk = 12.5;
      break;
    case F33_333:
      i_val = 10;
      d_Tlines = sensor.sensor_line_time[4];
      d_RefClk = 16.666;
      break;
    case F40:
      i_val = 11;
      d_Tlines = sensor.sensor_line_time[5];
      d_RefClk = 20;
      break;
    case F50:
      i_val = 0;
      d_Tlines = sensor.sensor_line_time[6];
      d_RefClk = 12.5;
      break;
    case F66_666:
      i_val = 2;
      d_Tlines = sensor.sensor_line_time[7];
      d_RefClk = 16.666;
      break;
    case F80:
      i_val = 3;
      d_Tlines = sensor.sensor_line_time[8];
      d_RefClk = 20.0;
      break;
    default: // 25 MHz
      i_val = 8;
      d_Tlines = sensor.sensor_line_time[3];
      d_RefClk = 12.5;
  }
  capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY] = i_val;

  // Send digital and analog gains
  i_val = sensor.digital_gain;
  i_val = i_val + 64 * (sensor.analog_gain);
  capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN] = i_val;

  // Send digital offset
  capture_params.registers[REGISTERS::DIGITAL_OFFSET] = sensor.digital_offset;

  double d_Tblankfactor;
  // Send Exposition
  capture_params.registers[REGISTERS::EXPOSITION] = 49;

  // Send resolution
  // 1st colon
  capture_params.registers[REGISTERS::RES_FIRST_COLUMN] = sensor.first_colon / 4;
  // Number of colons
  capture_params.registers[REGISTERS::RES_NUMBER_OF_COLUMNS] = sensor.image_width / 8;
  // 1st line
  capture_params.registers[REGISTERS::RES_FIRST_LINE] = sensor.first_line / 4;
  // Number of lines
  capture_params.registers[REGISTERS::RES_NUMBER_OF_LINES] = sensor.image_height / 8;

  // Adjust FPS
  double d_TFps, d_Texpo, d_Tread;
  double d_LinesBlank, d_Tblank, i_Tblank;
  int i_LinesRead, i_LinesBlank;

  // Frame period
  d_TFps = 1e6 / (sensor.frames_per_second);

  if (sensor.shutter_mode == DIFFERENTIAL)
    d_Texpo = 0;
  else if (sensor.shutter_mode == GLOBAL)
    d_Texpo = 48;
  else
    d_Texpo = (0 + 1) * 100;

  d_Texpo = 100;
  d_Tread = d_TFps - d_Texpo;

  i_LinesRead = (sensor.first_line) + 1;
  d_LinesBlank = (d_Tread / d_Tlines) - i_LinesRead;
  i_LinesBlank = floor(d_LinesBlank / 4) * 4;
  d_Tblank = d_TFps - ((i_LinesRead + i_LinesBlank) * d_Tlines + d_Texpo);

  if ((sensor.sensor_number == 803) || (sensor.sensor_number == 806) || (sensor.sensor_number == 902)
      || (sensor.sensor_number == 1001) || (sensor.sensor_number == 1101) || (sensor.sensor_number == 1104)
      || (sensor.sensor_number == 1201))
    d_Tblankfactor = 16;
  else
    d_Tblankfactor = 32;

  i_Tblank = floor(d_Tblank * d_RefClk / d_Tblankfactor);

  i_val = i_LinesBlank / 4;
  capture_params.registers[REGISTERS::LINES_BLANK] = 11;

  i_val = i_Tblank;
  capture_params.registers[REGISTERS::T_BLANK] = 18;

  // Noise compensation
  capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION] = sensor.fpn_correction;

  capture_params.bayer_OpenCV_code = sensor.bayer_OpenCV_code;
  capture_params.capture_timeout_ms = sensor.capture_timeout_ms;
  capture_params.has_bayer_filter = sensor.has_bayer_filter;
}

// Helper to get sensor definitions / parameters
SensorDefinitionParameters getSensorDefinitionParameters(const std::string sensor_name,
                                                         const std::string preset = "")
{
  SensorDefinitionParameters sensor;

  if (sensor_name.find("NSC1003") != std::string::npos && preset == "")
  {
    sensor.sensor_name = "NSC1003";
    sensor.sensor_number = 1003;
    sensor.shutter_mode = SHUTTER_MODE::GLOBAL;
    sensor.trigger_mode = TRIGGER_MODE::NO_EXTERNAL_SIGNAL;
    sensor.sensor_width = 1288;
    sensor.sensor_height = 1032;
    sensor.image_width = 1280;
    sensor.image_height = 1024;
    sensor.first_colon = 4;
    sensor.first_line = 4;
    sensor.sensor_line_time[0] = 1308 / 12.5;
    sensor.sensor_line_time[1] = 1308 / 16.666;
    sensor.sensor_line_time[2] = 1308 / 20.0;
    sensor.sensor_line_time[3] = 1308 / 25.0;
    sensor.sensor_line_time[4] = 1308 / 33.333;
    sensor.sensor_line_time[5] = 1308 / 40.0;
    sensor.sensor_line_time[6] = 1308 / 50.0;
    sensor.sensor_line_time[7] = 1308 / 66.666;
    sensor.sensor_line_time[8] = 1308 / 80.0;
    sensor.pixel_depth = 14;
    sensor.bayer_OpenCV_code = cv::COLOR_BayerBG2BGR_VNG;
    sensor.capture_timeout_ms = 240;
    sensor.pixel_clock_frequency = PIXEL_CLOCK_FREQUENCY_MHZ::F20;
    sensor.analog_gain = ANALOG_GAIN::DECIBELS_0;
    sensor.digital_gain = DIGITAL_GAIN::G1;
    sensor.digital_offset = 0;
    sensor.frames_per_second = 20;
    sensor.fpn_correction = FIXED_PATTERN_NOISE_CORRECTION::FPN_0_1;

    if (sensor_name.find("RGB") != std::string::npos)
      sensor.has_bayer_filter = true;
    else
      sensor.has_bayer_filter = false;
  }
  else
    throw std::runtime_error("Sensor/preset not recognized!");

  return sensor;
}

#endif
