#include <iostream>
#include <stdexcept>
#include <cyusb.h>
#include <chrono>
#include <thread>
#include <mutex>
#include <atomic>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "presets.hpp"

using std::cout;
using std::cerr;
using std::endl;

const unsigned VR_SetEP6H = 0xD0;
const unsigned VR_SetEP6L = 0xD1;
const unsigned VR_IO_Control = 0xD3;

unsigned sensor_width_;
unsigned sensor_height_;
int packet_length_;
cyusb_handle *handle_;
int endpoint_;
std::mutex buffer_mutex_;
uchar *buffer_;
std::vector<short> hist_AGC_;
const unsigned hist_total_max_ = 16383;

void throw_error(std::string error)
{
  cyusb_release_interface(handle_, 0);
  cyusb_detach_kernel_driver(handle_, 0);
  cyusb_close();
  throw std::runtime_error(error);
}

int sendUSBCommand(int nRequest,
                   int data)
{
  int bulk_transferred;
  unsigned char *buf = new unsigned char[0];
  bulk_transferred = cyusb_control_transfer(handle_, 0x40, nRequest, data, 0x00, buf, 0, 0);
  return bulk_transferred;
}

int sendDataToRegister(int RegAddr,
                       int RegVal)
{
  int data_transferred;
  unsigned short data;
  data = RegAddr * 256 + RegVal;
  data_transferred = sendUSBCommand(VR_IO_Control, data);
  return data_transferred;
}

void updateCameraCaptureParameters(const CaptureParameters &capture_params)
{
  sendDataToRegister(REGISTERS::SHUTTER_TRIGGER_MODES, capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES]);
  sendDataToRegister(REGISTERS::PIXEL_CLOCK_FREQUENCY, capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY]);
  sendDataToRegister(REGISTERS::DIGITAL_ANALOG_GAIN, capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN]);
  sendDataToRegister(REGISTERS::DIGITAL_OFFSET, capture_params.registers[REGISTERS::DIGITAL_OFFSET]);
  sendDataToRegister(REGISTERS::EXPOSITION, capture_params.registers[REGISTERS::EXPOSITION]);
  sendDataToRegister(REGISTERS::LINES_BLANK, capture_params.registers[REGISTERS::LINES_BLANK]);
  sendDataToRegister(REGISTERS::T_BLANK, capture_params.registers[REGISTERS::T_BLANK]);
  sendDataToRegister(REGISTERS::RES_FIRST_COLUMN, capture_params.registers[REGISTERS::RES_FIRST_COLUMN]);
  sendDataToRegister(REGISTERS::RES_NUMBER_OF_COLUMNS, capture_params.registers[REGISTERS::RES_NUMBER_OF_COLUMNS]);
  sendDataToRegister(REGISTERS::RES_FIRST_LINE, capture_params.registers[REGISTERS::RES_FIRST_LINE]);
  sendDataToRegister(REGISTERS::RES_NUMBER_OF_LINES, capture_params.registers[REGISTERS::RES_NUMBER_OF_LINES]);
  // UNUSED_1 is... not used.
  sendDataToRegister(REGISTERS::FPN_NOISE_COMPENSATION,
                     capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION]);

  /*
   cout << "Shutter trigger modes " << endl << "\tR = " << REGISTERS::SHUTTER_TRIGGER_MODES << "\n\tV = "
   << capture_params.registers[REGISTERS::SHUTTER_TRIGGER_MODES] << endl;
   cout << "Pixel clock frequency" << endl << "\tR = " << REGISTERS::PIXEL_CLOCK_FREQUENCY << "\n\tV = "
   << capture_params.registers[REGISTERS::PIXEL_CLOCK_FREQUENCY] << endl;
   cout << "Digital analog gain" << endl << "\tR = " << REGISTERS::DIGITAL_ANALOG_GAIN << "\n\tV = "
   << capture_params.registers[REGISTERS::DIGITAL_ANALOG_GAIN] << endl;
   cout << "Digital offset" << endl << "\tR = " << REGISTERS::DIGITAL_OFFSET << "\n\tV = "
   << capture_params.registers[REGISTERS::DIGITAL_OFFSET] << endl;
   cout << "Exposition" << endl << "\tR = " << REGISTERS::EXPOSITION << "\n\tV = "
   << capture_params.registers[REGISTERS::EXPOSITION] << endl;
   cout << "Unknown 1" << endl << "\tR = " << REGISTERS::LINES_BLANK << "\n\tV = "
   << capture_params.registers[REGISTERS::LINES_BLANK] << endl;
   cout << "Unknown 2" << endl << "\tR = " << REGISTERS::T_BLANK << "\n\tV = "
   << capture_params.registers[REGISTERS::T_BLANK] << endl;
   cout << "Resolution - First column" << endl << "\tR = " << REGISTERS::RES_FIRST_COLUMN << "\n\tV = "
   << capture_params.registers[REGISTERS::RES_FIRST_COLUMN] << endl;
   cout << "Resolution - number of columns" << endl << "\tR = " << REGISTERS::RES_NUMBER_OF_COLUMNS
   << "\n\tV = "
   << capture_params.registers[REGISTERS::RES_NUMBER_OF_COLUMNS] << endl;
   cout << "Resolution - First line" << endl << "\tR = " << REGISTERS::RES_FIRST_LINE << "\n\tV = "
   << capture_params.registers[REGISTERS::RES_FIRST_LINE] << endl;
   cout << "Resolution - Number of lines" << endl << "\tR = " << REGISTERS::RES_NUMBER_OF_LINES << "\n\tV = "
   << capture_params.registers[REGISTERS::RES_NUMBER_OF_LINES] << endl;
   cout << "FPS noise compensation" << endl << "\tR = " << REGISTERS::FPN_NOISE_COMPENSATION << "\n\tV = "
   << capture_params.registers[REGISTERS::FPN_NOISE_COMPENSATION] << endl;
   cout << endl;
   */
}

int main(int argc,
         char *argv[])
{
  CaptureParameters capture_parameters_;
  if (!getCaptureParameters(capture_parameters_, "NSC1003_BW", "rolling_33fps"))
  {
    cerr << "Could not get capture parameters" << endl;
    return 1;
  }

  sensor_width_ = capture_parameters_.registers[RES_NUMBER_OF_COLUMNS] * 8;
  sensor_height_ = capture_parameters_.registers[RES_NUMBER_OF_LINES] * 8;
  packet_length_ = sensor_width_ * sensor_height_ * 2;
  buffer_ = new uchar;

  hist_AGC_.resize(hist_total_max_ + 1);

  int device_count = cyusb_open();
  if (device_count == 0)
  {
    cyusb_close();
    throw std::runtime_error("No device found! Make sure a camera is plugged-in");
  }
  else if (device_count > 1)
    cout << "Multiple device found, using the first in the list" << endl;

  for (int i(0); i < device_count; ++i)
  {
    cyusb_handle* handle_temp(cyusb_gethandle(i));
    if (!handle_temp)
    {
      cout << "Device " << i << " did not return a valid handle" << endl;
      continue;
    }

#ifdef DEBUG
    std::cout << "Camera " << i << std::endl <<
    "Vendor ID = " << cyusb_getvendor(handle_temp) << std::endl <<
    "Product = " << cyusb_getproduct(handle_temp) << std::endl <<
    "Bus number = " << cyusb_get_busnumber(handle_temp) << std::endl <<
    "Device address = " << cyusb_get_devaddr(handle_temp) << std::endl;
#endif
  }

  // Use first device found
  handle_ = cyusb_gethandle(0);
  if (cyusb_kernel_driver_active(handle_, 0) == 1)
  {
    cout << "Kernel driver active: trying to detach driver...";
    if (cyusb_detach_kernel_driver(handle_, 0) != 0)
      throw_error("Could not detach kernel driver");
  }

  if (cyusb_claim_interface(handle_, 0) != 0)
  {
    throw_error("Could not claim interface. Is there another program using the camera?");
  }

  updateCameraCaptureParameters(capture_parameters_);

  int rvalue(sendUSBCommand(VR_SetEP6H, 0));
  if (rvalue != 0)
  {
    // FIXME Always fail
    std::string error(libusb_error_name(rvalue));
    throw_error("Error sending USB command VR_SetEP6H. libusb error: " + error);
  }

  if (cyusb_getproduct(handle_) == 4099) // USB 2 case
    endpoint_ = 0x82;
  else if (cyusb_getproduct(handle_) == 241) // USB 3 case
    endpoint_ = 0x83;
  else
  {
    throw_error("Product not recognized!");
  }

  buffer_ = new uchar[sensor_width_ * sensor_height_ * 2];
  return 0;
}

